---
title: "Hi, I'm Constanza and this is my portfolio."
button1:
  label: "My biography"
  link: "posts/biography"
button2:
  label: "My projects"
  link: "posts/projects"
button3:
  label: "CV"
  link: "/about"

gmail: "constanzamrsm@gmail.com"
instagram: "thegrangerneer"

copyright: "2023 My Portfolio"
---


On this website you will find:

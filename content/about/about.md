---
title: About me
items:
  - "Name: Constanza Maria"
  - "Age: 24"
  - "Phone: +55 (XX) XXXX-XXXX"
  - "Experience: XX years"
  - "Address: São Paulo, SP"
cvDownloadLink: /seu_cv.pdf

gmail: "constanzamrsm@gmail.com"
instagram: "thegrangerneer"

copyright: "2023 My Portfolio"
---

My name is Constanza Maria Mariano, I am a Mechatronics Engineering student at PoliUSP. 
With a strong background in CAD and CAM, I have honed my skills in design and manufacturing. 
I have applied my coding expertise to various projects and I am proficient in programming languages such as Python, C, and C++. My dedication to academic excellence and passion for technology earned me a full scholarship to the prestigious Weizmann Institute's summer school in Rehovot, Israel, where I delved into the realm of atomic physics. My ultimate aspiration is to lead and innovate in the field of mechatronic systems, driving me to dream big and make my mark in the world of technology with more inclusive leaderships. 

- Research interest

I'm interested in precision engineering and optical systems. In a near future I would like to study more about quantum computing.
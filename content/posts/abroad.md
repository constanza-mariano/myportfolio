---
title: "My Experiences Abroad"

gmail: "constanzamrsm@gmail.com"
instagram: "thegrangerneer"

copyright: "2023 My Portfolio"
---

The day I spent abroad was the best day of my life. I was very proud to be able to organize a trip to anywhere I could reach by trains and airplanes. It was magical to experience this sense of freedom for some time. I would say it was crucial for building my character and understanding myself.

Growing up, my family always struggled with financial management, and therefore, going abroad meant a dream very far from my reality. However, once life was very kind to me and granted me this opportunity, I am proud that I was able to enjoy it for the time I had.

I believe that the international experience has impacted me so much that I want it to be a 100% part of my professional life. I enjoy learning languages, and I am fluent in English. Right now, I am a German-speaking learner. I hope my next trip won't take so long to take place again. 




---
title: "Biography"

gmail: "constanzamrsm@gmail.com"
instagram: "thegrangerneer"

copyright: "2023 My Portfolio"
---

I was born and raised in Rio de Janeiro, and from a young age, I harbored a keen interest in STEM. This passion led me to pursue a degree in engineering. The allure of technology and its myriad possibilities fascinated me deeply. This enthusiasm only intensified when I enrolled in the engineering program at USP. Despite the initial challenges, I persevered, constantly devising creative strategies to manage the demanding workload that the university presented.

To cope with the academic pressures, I've explored various hobbies. Going for walks, rowing, running, reading books, and baking gluten-free cakes have become integral parts of my routine, providing a welcome escape from the rigorous demands of my studies.

In addition to my academic pursuits, I am in a long-distance relationship with Benedict, whom I met during my exchange program in Germany. While the physical distance poses its challenges, modern technology, particularly FaceTime, has been instrumental in bridging the gap. Despite the miles between us, staying connected has become an essential part of our relationship.
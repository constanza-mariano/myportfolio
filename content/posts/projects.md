---
title: "My Projects"
date: 2023-10-02
experiences:
  - title: "Student Assistant"
    company: "Fraunhofer IPT"
    date: "September 2021 - March 2023"
    description: | 
        Ultra-Precision technology machinist: ductile machining of brittle hard materials. The tasks involve independent studies, the performance of CAM programming, CAD-Design, and performing of experiments (tools, tool holders, workbench setup). Evaluation and documentation of results (measurements, photos, videos, plots, presentations). I also developed there my bachelor thesis on the topic on tool wear investigation while ultra-precision grinding of brittle parts."
  - title: "Bolsista PET (Programa de Educação Tutorial)"
    company: "PET Mecatrônica"
    date: "June 2019 - Today"
    description: | 
        The program inserts undergraduate students in educational projects with the goal of applying practical knowledge and broadening their academic education, based on the inseparability between teaching, research, and extension.
  - title: "Summer Internship"
    company: "Weizmann Institute of Science"
    date: "Jul 2019 - Aug 2019"
    description: |
        The Dr Bessie F. Lawrence ISSI program awards students from around the world for an immersive experience of scientific research at the Institute. I developed a camera lens setup using SolidWorks that allowed remote visualization of the RHEED output screen, a technique used to monitor the process of growing pristine material in the MBE (Molecular Beam Epitaxy) equipment. These materials would later be studied in the STM (Scanning Tunneling Microscope) to make conclusions from its atomic properties.

gmail: "constanzamrsm@gmail.com"
instagram: "thegrangerneer"
copyright: " 2023 My Portfolio"
---

Here are is the project on technology I have engaged in.